
-- Дамп структуры для таблица story.story
CREATE TABLE IF NOT EXISTS `story` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `story` text,
  `email` varchar(255) DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы story.story: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `story` DISABLE KEYS */;
INSERT IGNORE INTO `story` (`id`, `title`, `story`, `email`, `post_date`) VALUES
	(1, 'Go', 'Money makes the world go round!', 'email@email.com', '2016-02-28 17:17:49'),
	(2, 'Bad money', 'Money is the root of all evil', '', '2016-02-28 17:17:49'),
	(3, 'test', 'test', 'test.ru', '2016-03-06 17:54:52');
/*!40000 ALTER TABLE `story` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
