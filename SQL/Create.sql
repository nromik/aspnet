Show databases;
create database Story;

use Story;

Create table story (
	id int primary key auto_increment,
	title varchar(255),
	story text,
	email varchar(255),
	post_date datetime) default charset=utf8;
	
Alter table story add column status enum('wait','show') default 'wait';
Alter table story drop column status;
Alter table story add column status enum('wait','show','drop') default 'wait';

explain story;

show create table story;

create table `user`(
    id int primary key auto_increment,
    login varchar(50),
    email varchar(50),
    pass varchar(50),
    `status` int default 0
) default charset=utf8;

insert into user (login, pass, status) values ('admin', 'pass', 1); 

select * from user;



-- truncate table story;

INSERT INTO story (title, story, email, post_date)
VALUES ('Go', 'Mone makes the world go round!', 'email@email.com', NOW());

INSERT INTO story
	SET title = 'Bad money',
		 story = 'Money is the root of all evil',
		 email = '',
		 post_date = NOW();

-- Вывести все истории от новой к старой
select id, title, story email, post_date 
		from story
		ORDER BY post_date DESC;
		
-- Вывести случайнуюю историю.	
select id, title, story email, post_date
		from story
		ORDER BY RAND();
		LIMIT 1;
		
select id, title, story email, post_date 
		from story
		where id = '2';	
		
select id, title, story email, post_date 
		from story
		WHERE title LIKE '%is%'
			OR story LIKE '%is%'
		ORDER BY post_date;	
		
			 