using System;

namespace AspNet.Models
{
    public class SomeSqlException : ApplicationException
    {

        public SomeSqlException() { }

        public SomeSqlException(string message) : base(message) { }

        public SomeSqlException(string message, Exception ex) : base(message) { }

        // ����������� ��� ��������� ������������ ����
        protected SomeSqlException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext contex)
            : base(info, contex) { }

        public string Query { get; set; }
    }
}