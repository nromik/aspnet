﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using MySql.Data.MySqlClient;

namespace AspNet.Models
{
    public static  class SomeSql
    {
        private static MySqlConnection connection;

        static SomeSql()
        {
            InitializationConnectionToDataBase();
        }

        private static void InitializationConnectionToDataBase()
        {
            MySqlConnectionStringBuilder csb = new MySqlConnectionStringBuilder(
                    WebConfigurationManager.ConnectionStrings["connection"].ConnectionString);
            csb.Pooling = true;
            connection = new MySqlConnection(csb.ConnectionString);
        }

        public static DataTable Select(string myQuery)
        {
            using (connection)
            {
                try
                {
                    connection.Open();

                    DataTable table = new DataTable();
                    MySqlCommand cmd = new MySqlCommand(myQuery, connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    table.Load(reader);
                    return table;
                }
                catch (MySqlException exInner)
                {
                    var ex = new SomeSqlException(exInner.Message,exInner)
                    {
                        Query = myQuery
                    };
                    throw ex;
                }
            }
        }


        public static string Scalar(string myQuery)
        {
            using (connection)
            {
                try
                {
                    connection.Open();

                    DataTable table = new DataTable();
                    MySqlCommand cmd = new MySqlCommand(myQuery, connection);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    table.Load(reader);
                    if (table.Rows.Count == 0)
                        return "";
                    return table.Rows[0][0].ToString();
                }
                catch (MySqlException exInner)
                {
                    var ex = new SomeSqlException(exInner.Message, exInner)
                    {
                        Query = myQuery
                    };
                    throw ex;
                }
            }
        }

        public static long Insert(string myQuery)
        {
            using (connection)
            {
                try
                {
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(myQuery, connection);
                    cmd.ExecuteNonQuery();
                    return cmd.LastInsertedId;

                }
                catch (MySqlException exInner)
                {
                    var ex = new SomeSqlException(exInner.Message, exInner)
                    {
                        Query = myQuery
                    };
                    throw ex;
                }
            }
        }

        public static int Update(string myQuery)
        {
            using (connection)
            {
                try
                {
                    connection.Open();

                    MySqlCommand cmd = new MySqlCommand(myQuery, connection);
                    cmd.ExecuteNonQuery();
                    return cmd.ExecuteNonQuery();

                }
                catch (MySqlException exInner)
                {
                    var ex = new SomeSqlException(exInner.Message, exInner)
                    {
                        Query = myQuery
                    };
                    throw ex;
                }
            }
        }

        static public string AddSlashes(string text)
        {
            try
            {
                return text.Replace("'", "\\'").Replace("\"", "\\\"").Replace("\\", "\\\\");
            }
            catch (NullReferenceException)
            {
                return "";

            }
        }


    }
}