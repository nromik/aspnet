﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AspNet.Models
{
    public class User
    {
        [Required(ErrorMessage = "Введите логин")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        public string Password { get; set; }

         public string Status { get; private set; }



        public User()
        {
            Status = "";
        }
        public void Checklogin()
        {
            Status = SomeSql.Scalar(
                "SELECT COUNT(*)" +
                "  FROM user" +
                " WHERE login = '" + SomeSql.AddSlashes(Login) +
                "'  AND  pass = '" + SomeSql.AddSlashes(Password) +
                "'  AND status > 0");
        }
    }
}