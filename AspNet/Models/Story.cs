﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AspNet.Models
{
    public class Story
    {
        public string Id { get; private set; }

        [Required(ErrorMessage = "Введите заголовок")]
        public string Title{ get; set; }

        [Required(ErrorMessage = "Введите текст истории")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Введите адрес эл. почты")]
        [RegularExpression(@"([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}",
            ErrorMessage = "Адрес эл. почты указан некорректно")]
        public string Email { get; set; }

        public string Ename { get; private set; }
        public string PostDate { get; private set; }
        public Story[] List { get; private set; }

        private string query;
        private DataTable table ;
        

        public void GenerateList(string myLimit)
        {
            int limit;
            if(!int.TryParse(myLimit,out limit)) limit = 10;

            query = "SELECT id, title, story, email, post_date" +
                    "  FROM story " +
                    " WHERE status = 'show'" +
                    " ORDER BY id DESC " +
                    " LIMIT " + limit;

            table = SomeSql.Select(query);

            List = new Story[table.Rows.Count];

            for (int j = 0; j < List.Length; j++)
            {
                List[j] = new Story {table = table};
                List[j].ExtractRow(j);

            }
        }

        public void Add()
        {
            query = "INSERT INTO story(title, story, email, post_date) " +
                    "VALUES    ('" + SomeSql.AddSlashes(Title) +
                            "', '" + SomeSql.AddSlashes(Text) +
                            "', '" + SomeSql.AddSlashes(Email) +
                            "', NOW())";

            Id = SomeSql.Insert(query).ToString();
        } 

        public void Random()
        {
            query = "SELECT id, title, story, email, post_date" +
                    "  FROM story " +
                    " ORDER BY RAND() " +
                    " LIMIT 1";

            ExtractQuery();
            ExtractRow();
        }

        public void Number(long id)
        {
            query = "SELECT id, title, story, email, post_date " +
                    "  FROM story " +
                    " WHERE id = '" + id + "'";

            ExtractQuery();
            ExtractRow();
        }

        private void ExtractQuery()
        {
            table = SomeSql.Select(query);
        }

        private void ExtractRow(int numberRow = 0)
        {
            if (!(table.Rows.Count > numberRow))
            {
                RecordFieldNotFoundStory();
                return;
            }
            RecordResultsInTheField(numberRow);
        }

        private void RecordFieldNotFoundStory()
        {
            Id = "---";
            Title = "Not found Story";

#if(DEBUG)  
            Text = "Not found Story \n Query: " + query;
#endif

#if(!DEBUG)
            Text = "Not found Story ";
#endif

            Email = "";
            Ename = "";
            PostDate = "----.--.--";
        }

        private void RecordResultsInTheField(int numberRow)
        {
            Id =    table.Rows[numberRow]["id"].ToString();
            Title = table.Rows[numberRow]["title"].ToString();
            Text =  table.Rows[numberRow]["story"].ToString();
            Email = table.Rows[numberRow]["email"].ToString();
            int posAt = Email.IndexOf('@');
            if (posAt == -1)
                Ename = Email;
            else
                Ename = Email.Substring(0, posAt);
            PostDate = ((DateTime)table.Rows[numberRow]["post_date"]).ToString("yyyy-MM-dd"); // простое не правельное решение - не разрешает локализацию.
        }

        public bool SelectWaitStory()
        {
            query = "SELECT id, title, story, email, post_date " +
                    "  FROM story " +
                    " WHERE status = 'wait'" +
                    //"ORDER BY post_date ASC" +
                    "LIMIT 1";
            
            ExtractQuery();

            if (table.Rows.Count == 0)
                return false;

            ExtractRow();
            return true;
        }

        public void Approve(string id)
        {
            query = "UPDATE story " +
                    "   SET status = 'show'" +
                    " WHERE id = '" + SomeSql.AddSlashes(id) + "';";
            SomeSql.Update(query);
        }

        public void Decline(string id)
        {
            query = "UPDATE story " +
                    "   SET status = 'drop'" +
                    " WHERE id = '" + SomeSql.AddSlashes(id) + "';";
            SomeSql.Update(query);
        }
    }
}