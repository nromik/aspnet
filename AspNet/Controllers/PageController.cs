﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspNet.Models;

namespace AspNet.Controllers
{
    public class PageController : Controller
    {
       
        public ActionResult Index()
        {
            //return Redirect("/story/number/2");
           
            Story story = new Story();
            
            try
            {
                story.GenerateList("3");
                return View(story);
            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
            catch (Exception ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
        }

        public ActionResult About()
        {
            return View();

        }
    }
}