﻿using System;
using AspNet.Models;
using System.Web.Mvc;

namespace AspNet.Controllers
{
    public class UserController : Controller
    {
        private User user;

        public UserController()
        {
            user = new User();
        }

        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                return View(user);
            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
            
        }


        [HttpPost]
        public ActionResult Index(User userPost)
        {
            user.Login = userPost.Login;
            user.Password = userPost.Password;
            
            try
            {
                userPost.Checklogin();
                if (userPost.Status == "1")
                    Session["admin"] = "1";

                return View(userPost);

            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
            catch (Exception ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
        }


        public ActionResult Logout(string s)
        {
            try
            {

            user = new User();
            Session["admin"] = "";
            return View("Index", user);

            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
            catch (Exception ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
        }

        public ActionResult Checker()
        {
            if ((string) Session["admin"] != "1")
                return Redirect("Index");
            Story story = new Story();
            ViewBag.IsChecker = story.SelectWaitStory();

            return View(story);
        }

        public ActionResult Approve()
        {
            if ((string) Session["admin"] != "1")
                return Redirect("Index");
            string id = (RouteData.Values["id"] ?? "").ToString();
            Story story = new Story();
            story.Approve(id);
            return Redirect("~/User/Checker");
        }

        public ActionResult Decline()
        {
            if ((string) Session["admin"] != "1")
                return Redirect("Index");
            string id = (RouteData.Values["id"] ?? "").ToString();
            Story story = new Story();
            story.Decline(id);
            return Redirect("~/User/Checker");
        }
    }
}