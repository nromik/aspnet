﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspNet.Models;

namespace AspNet.Controllers
{
    public class ErrorConrtoller : Controller
    {
        public ActionResult ErrorActionResult(SomeSqlException ex)
        {

#if(!DEBUG)
            return View("~/Views/Error/Error.cshtml");
#endif

#if(DEBUG)
            PreparationPageError(ex);
            return View("~/Views/Error/ErrorDebug.cshtml");
#endif
        }

        public ActionResult ErrorActionResult(Exception ex)
        {

#if(!DEBUG)
            return View("~/Views/Error/Error.cshtml");
#endif

#if(DEBUG)
            PreparationPageError(ex);
            return View("~/Views/Error/ErrorDebug.cshtml");
#endif
        }

        private void PreparationPageError(SomeSqlException ex)
        {
            ViewBag.Query = ex.Query;
            ViewBag.Exception = ex.GetType();
            ViewBag.Message = ex.Message;
            ViewBag.TargetSite = ex.TargetSite;
            string[] stackTraces;
            try
            {
                stackTraces = ex.StackTrace.Replace(@") в ", ") -> ").Replace(@" в ", "|").Split('|');
            }
            catch (NullReferenceException)
            {
                stackTraces = new[] {""};
            }
            ViewBag.StackTraces = stackTraces;
            ViewBag.InnerException = ex.InnerException;
        }

        private void PreparationPageError(Exception ex)
        {
            ViewBag.Query = "Exception ex";
            ViewBag.Exception = ex.GetType();
            ViewBag.Message = ex.Message;
            ViewBag.TargetSite = ex.TargetSite;
            string[] stackTraces;
            try
            {
                //stackTraces = ex.StackTrace.Replace(@") в ", ") -> ").Replace(@" в ", "|").Split('|');
                stackTraces = ex.StackTrace.Replace(@") at ", ") -> ").Replace(@" at ", "|").Split('|');
            }
            catch (NullReferenceException)
            {
                stackTraces = new[] { "" };
            }
            ViewBag.StackTraces = stackTraces;
            ViewBag.InnerException = ex.InnerException;
        }


    }
}