﻿using AspNet.Models;
using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AspNet.Controllers
{
    public class StoryController : Controller
    {

        private Story story;

        public StoryController()
        {
            story = new Story();
        }

        // GET: Story
        public ActionResult Index()
        {
            return Redirect("/page");
        }

        public ActionResult Random()
        {

            try
            {
                story.Random();
                return View("Number", story);
            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
        }

        [HttpGet]
        public ActionResult Add()
        {
            story.Title ="";
            story.Text = "";
            story.Email ="";
            try
            {
                return View(story);
            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
            
        }

        [HttpPost]
        public ActionResult Add(Story post)
        {
            if (!ModelState.IsValid) // 
                return View(post);

            story.Title = post.Title;
            story.Text  = post.Text;
            story.Email = post.Email;

            try
            {
                story.Add();
                return Redirect("/story/number/" + story.Id);
            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
        }


        public ActionResult Number()
        {
            long id;
            if(!long.TryParse(RouteData.Values["id"].ToString(), out id))
                return Redirect("/page");

            try
            {
                story.Number(id);
                return View(story);
            }
            catch (SomeSqlException ex)
            {
                return new ErrorConrtoller().ErrorActionResult(ex);
            }
        }

        private void PreparationPageError(SomeSqlException ex)
        {
            ViewBag.Query = ex.Query;
            ViewBag.Exception = ex.GetType();
            ViewBag.Message = ex.Message;
            ViewBag.TargetSite = ex.TargetSite;
            string[] stackTraces;
            try
            {
                stackTraces = ex.StackTrace.Replace(@") в ", ") -> ").Replace(@" в ", "|").Split('|');
            }
            catch (NullReferenceException)
            {
                stackTraces = new[] { "" };
            }
            ViewBag.StackTraces = stackTraces;
            ViewBag.InnerException = ex.InnerException;
        }

    }
}